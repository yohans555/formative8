package Formative;

import java.io.FileOutputStream;
import java.io.IOException;

public class Task1dan2 {
    public static void main(String[] args) throws IOException {
        String text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas dictum nisl sed mi tempus tristique. Suspendisse eu convallis nunc. In hac habitasse platea dictumst. Mauris placerat quis nulla ac rhoncus. Mauris blandit nisl sit amet facilisis rhoncus. Suspendisse et posuere neque. Quisque sapien lectus, posuere id nulla eget, convallis mollis tellus. Duis convallis sapien ut nunc facilisis fermentum. Nulla placerat massa quis mattis rutrum. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aenean scelerisque luctus eros ac volutpat..";
        
        //Nomor1
        String replaced_text = text
        .replaceAll("L", "7")
        .replaceAll("i", "1")
        .replaceAll("p", "8")
        .replaceAll("0", "9")
        .replaceAll("s", "5")
        ;
        System.out.println(replaced_text);

        //Nomor 2
        FileOutputStream day821 = new FileOutputStream("day821.txt");        
        FileOutputStream day822 = new FileOutputStream("day822.txt");


        String withoutalphabet = replaced_text.replaceAll("\\D+", "");
        //System.out.println(withoutalphabet);

        String withoutnumber = replaced_text.replaceAll("[\\s\\.\\d]+", "");
        //ystem.out.println(withoutnumber);

        day821.write(withoutalphabet.getBytes());
        day822.write(withoutnumber.getBytes());
        day821.close();
        day822.close();
    }

}
