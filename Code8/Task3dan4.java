package Formative;

import java.time.LocalDate;
import java.util.Scanner;

public class Task3dan4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        //Masukan Tanggal Registrasi
        System.out.println("Masukan Tahun daftar: ");
        int year = sc.nextInt() ;
        System.out.println("\nMasukan Bulan daftar: ");
        int month = sc.nextInt();
        System.out.println("\nMasukan hari daftar: ");
        int dayOfMonth = sc.nextInt();

        //Mau Reminder berapa hari sebelum jatuh tempo?
        System.out.println("\nTentukan Reminder pembayaranmu (1-7): ");
        int reminder = sc.nextInt();
       
        //Apakah Anda member baru??
        System.out.println("\nApakah anda Member baru?: ");
        boolean newmember = sc.nextBoolean();

        LocalDate registerdate= LocalDate.of(year, month, dayOfMonth);
        LocalDate finishdate= registerdate.plusMonths(12);

        int counter = 1;
        while(registerdate.compareTo(finishdate)<0){
            if (!newmember) {
                System.out.println("ini reminder! sudah tanggal :"+ registerdate.plusDays(Math.negateExact(reminder))+
                "  Bayar Tagihan!\n");
                System.out.println("Bayaran untuk tanggal : "+ registerdate+ "   Sebesar : " +10000+"\n");
                registerdate = registerdate.plusMonths(1);
                
            } else {
                if(counter==2||counter==3||counter==4){
                    System.out.println("Bulan Ini masih Promo anda tidak perlu bayar\n");
                    registerdate = registerdate.plusMonths(1);
                    counter= counter +1;
                }else{
                    System.out.println("ini reminder! sudah tanggal :"+ registerdate.plusDays(Math.negateExact(reminder))+
                    "  Bayar Tagihan!\n");
                    System.out.println("Bayaran untuk tanggal : "+ registerdate+ "   Sebesar : " +10000+"\n");
                    registerdate = registerdate.plusMonths(1);
                    counter= counter +1;
                }
            }
            
        }sc.close();
    }
}
